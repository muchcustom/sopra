using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;



/// <summary>
/// Diese Klasse dient dazu eine TCP Verbindung zu einem Server herzustellen.
/// Es werden Methoden zum Erhalten und Versenden von Nachrichten zur Verf�gung gestellt.
/// </summary>



public class CreateTCPConnection : MonoBehaviour 
{
  	
    //Socket Verbindung und Thread zum Erhalten der Nachrichten
	private TcpClient socketConnection; 	
	private Thread clientReceiveThread; 	
  	
	
	// Use this for initialization 	
	void Start () 
	{
		ConnectToTcpServer();     
	}  	
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Space)) 
		{             
			SendMessage();         
		}     
	}  	



	//Verbindung zum Server wird hergestellt
	private void ConnectToTcpServer () 
	{
		//Falls das Verbinden nicht gelingt wird eine Exception auf der Konsole ausgegeben 		
		try 
		{  			
			clientReceiveThread = new Thread (new ThreadStart(ListenForData)); 			
			clientReceiveThread.IsBackground = true; 			
			clientReceiveThread.Start();  		
		} 		
		catch (Exception e) 
		{ 			
			Debug.Log("On client connect exception " + e); 		
		} 	
	}  	

	

	//Wenn die Verbindung steht wird im Hintergrund auf eintreffende Nachrichten gewartet
	private void ListenForData() 
	{ 	
        //Falls die Verbindung durch eine Exception abbricht wird er Error auf der Konsole ausgegeben
		try 
		{ 		

		    //Verbindung wird erstellt
			socketConnection = new TcpClient("localhost", 8052);  			
			Byte[] bytes = new Byte[1024];   
			
            //Die Schleife l�uft solange wie das Programm um Nachrichten immer empfangen zu k�nnen
			while (true) 
			{ 			
			
				//Stream Objekt um Daten zu lesen			
				using (NetworkStream stream = socketConnection.GetStream()) 
				{ 	
				
					int length; 	
					
					//Nachricht wird in ein Byte Array geschrieben			
					while ((length = stream.Read(bytes, 0, bytes.Length)) != 0) 
					{ 	
					
						var incommingData = new byte[length]; 						
						Array.Copy(bytes, 0, incommingData, 0, length); 
						
						//Byte Array wird zu String umgewandelt					
						string serverMessage = Encoding.ASCII.GetString(incommingData); 	

						//Nachricht wird auf Konsole ausgegeben DEBUG
						Debug.Log("server message received as: " + serverMessage); 					
					} 				
				} 			
			}         
		}         
		catch (SocketException socketException) 
		{             
			Debug.Log("Socket exception: " + socketException);         
		}     
	}  	

	
	//Nachricht wird an Server gesendet
	private void SendMessage() 
	{    
		//Wenn keine Verbindung besteht auch kein Versenden der Nachricht
		if (socketConnection == null) {             
			return;         
		}  		
		
        //Versuch Nachricht zu versenden | Bei Exception wird diese Ausgegeben
		try 
		{ 			
			// Stream Objekt zum schreiben der Nachricht	
			NetworkStream stream = socketConnection.GetStream(); 	
			
			if (stream.CanWrite)
			{
				//TestNachricht
				string clientMessage = "This is a message from one of your clients."; 	
				
				// String wird zu Byte Array           
				byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(clientMessage); 	
				
				// Byte Array wird in die SocketConnection geschrieben              
				stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);                 
				Debug.Log("Client sent his message - should be received by server");             
			}         
		} 		
		catch (SocketException socketException) 
		{             
			Debug.Log("Socket exception: " + socketException);         
		}     
	} 
}