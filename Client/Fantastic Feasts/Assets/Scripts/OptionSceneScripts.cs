using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OptionSceneScripts : MonoBehaviour
{

    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public AudioMixer audioMixer;

    public void setMusikVolume(float volume)
    {
        audioMixer.SetFloat("musikVolumen", volume);
    }
    public void setEffektVolumen(float volume)
    {
        audioMixer.SetFloat("effekteVolumen", volume);
}
    public void SetQuality (int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

}
