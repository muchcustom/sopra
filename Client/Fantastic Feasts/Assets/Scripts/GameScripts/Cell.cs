﻿using UnityEngine;
using System.Collections;

public enum CellType
{
    HÜTER, MITTELKREIS, MITTELFELD, MITTELLINIE, TORRING, LEER
}
public class Cell : MonoBehaviour
{
    public int x;
    public int y;
    public bool occupied;
    public CellType cellType;
}