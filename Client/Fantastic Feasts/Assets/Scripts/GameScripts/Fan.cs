﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum FanRole
{
    Goblins, Trolle, Elfen, Niffler, Wombats
}

public class Fan
{
    public Vector3Int Position { get; set; }
    public FanRole FanRole { get; set; }
    public bool Used { get; set; }
    public Tile Tile { get; set; }

    public Fan (FanRole fanRole, bool used)
    {
        FanRole = fanRole;
        Used = used;
    }

    void DoFanAction(FanRole fanRole)
    {
        switch (fanRole)
        {
            case FanRole.Goblins:
                DoGoblinAktion();
                break;
            case FanRole.Trolle:
                DoTrollAktion();
                break;
            case FanRole.Elfen:
                DoElfenAktion();
                break;
            case FanRole.Niffler:
                DoNifflerAktion();
                break;
            case FanRole.Wombats:
                DoWombatsAktion();
                break;
            default:
                //something went wrong, shit
                break;
        }

    }

    void DoGoblinAktion()
    {

    }
    void DoTrollAktion()
    {

    }

    void DoElfenAktion()
    {

    }
    void DoNifflerAktion()
    {

    }
    void DoWombatsAktion()
    {

    }
}