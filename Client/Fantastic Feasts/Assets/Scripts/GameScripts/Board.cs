﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Board : MonoBehaviour
{
    public Cell CellPrefab;

    // 2D List which stores all cells
    public List<List<Cell>> Cells;

    public Material blackMaterial;
    public Material whiteMaterial;
    public Material leerMaterial;
    public Material hueterMaterial;
    public Material torringMaterial;
    public Material mittelFeldMaterial;
    public Material mittelKreisMaterial;

    // Holds the different types of the field
    public static readonly CellType[,] CellTypes = {
        {CellType.LEER, CellType.LEER, CellType.LEER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.LEER, CellType.LEER, CellType.LEER},
        {CellType.LEER, CellType.LEER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.LEER, CellType.LEER},
        {CellType.LEER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.LEER},
        {CellType.LEER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.LEER},
        {CellType.HÜTER, CellType.HÜTER, CellType.TORRING, CellType.HÜTER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.HÜTER, CellType.TORRING, CellType.HÜTER, CellType.HÜTER},
        {CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELKREIS, CellType.MITTELKREIS, CellType.MITTELKREIS, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER},
        {CellType.HÜTER, CellType.HÜTER, CellType.TORRING, CellType.HÜTER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELKREIS, CellType.MITTELKREIS, CellType.MITTELKREIS, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.HÜTER, CellType.TORRING, CellType.HÜTER, CellType.HÜTER},
        {CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELKREIS, CellType.MITTELKREIS, CellType.MITTELKREIS, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER},
        {CellType.HÜTER, CellType.HÜTER, CellType.TORRING, CellType.HÜTER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.HÜTER, CellType.TORRING, CellType.HÜTER, CellType.HÜTER},
        {CellType.LEER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.LEER},
        {CellType.LEER, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.HÜTER, CellType.HÜTER, CellType.LEER},
        {CellType.LEER, CellType.LEER, CellType.HÜTER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.HÜTER, CellType.LEER, CellType.LEER},
        {CellType.LEER, CellType.LEER, CellType.LEER, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELLINIE, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.MITTELFELD, CellType.LEER, CellType.LEER, CellType.LEER},
    };

    public void Generate()
    {
        // Fill cellList 
        Cells = new List<List<Cell>>(GameManager.numberColumns);
        for (int y = 0 ; y < GameManager.numberRows; y++)
        {
            Cells.Add(new List<Cell>(GameManager.numberRows));
            for (int x = 0; x < GameManager.numberColumns; x++)
            {   
                Cells[y].Add(CreateCell(x, y));
            }
        }
    }

    Cell CreateCell(int posx, int posy)
    {
        Cell newCell = Instantiate(CellPrefab) as Cell;

        // Set cellType
        newCell.cellType = CellTypes[posy, posx];

        // Get scale of a cell
        float scale = CellPrefab.transform.localScale.x;

        // Decide color
        switch (CellTypes[posy, posx])
        {
            case CellType.LEER:
                newCell.GetComponent<Renderer>().material = leerMaterial;
                break;
            case CellType.MITTELFELD:
                newCell.GetComponent<Renderer>().material = mittelFeldMaterial;
                break;
            case CellType.MITTELKREIS:
                newCell.GetComponent<Renderer>().material = mittelKreisMaterial;
                break;
            case CellType.MITTELLINIE:
                newCell.GetComponent<Renderer>().material = whiteMaterial;
                break;
            case CellType.HÜTER:
                newCell.GetComponent<Renderer>().material = hueterMaterial;
                break;
            case CellType.TORRING:
                newCell.GetComponent<Renderer>().material = torringMaterial;
                break;
            default:
                newCell.GetComponent<Renderer>().material = whiteMaterial;
                break;
        }

        newCell.x = posx;
        newCell.y = posy;

        newCell.transform.parent = transform;
        newCell.transform.localPosition = new Vector3(posx * scale, -posy * scale, 0);

        newCell.name = "Board cell [" + posx * scale + "," + posy * scale + "]";

        return newCell;
    }
}