﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum BallType
{
    Snitch, Bludger, Quaffle
}

public class Ball
{
    public BallType BallType { get; set; }
    public Vector3Int Position { get; set; }
    public Tile Tile { get; set; }

    public Ball(BallType ballType, Vector3Int position, Tile tile)
    {
        BallType = ballType;
        Position = position;
        Tile = tile;
    } 
}
