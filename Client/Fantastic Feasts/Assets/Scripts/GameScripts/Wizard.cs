﻿using UnityEngine;
using UnityEngine.Tilemaps;

public enum WizardRole
{
    Hüter, Sucher, Treiber, Jäger
}

public class Wizard
{
    public int TeamNr { get; set; }
    public Vector3Int Position { get; set; }
    public WizardRole WizardRole { get; set; }
    public Besen Besen { get; set; }
    public string TeamRole { get; set; } // Stores the role with the according number, e.g. beater1
    public string Playername { get; set; }
    public string Gender { get; set; }
    public Tile Tile { get; set; }

    public Wizard(int teamNr, Vector3Int position, WizardRole wizardRole, Besen besen, string teamRole, string playername, string gender)
    {
        TeamNr = teamNr;
        Position = position;
        WizardRole = wizardRole;
        Besen = besen;
        TeamRole = teamRole;
        Playername = playername;
        Gender = gender;
    }
}
