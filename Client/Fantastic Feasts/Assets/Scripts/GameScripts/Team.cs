﻿using UnityEngine;
using System.Collections.Generic;

public enum Besen
{
    tinderblast, cleansweep11, comet260, nimbus2001, firebolt
}

public class Team
{
    public string TeamName { get; set; }
    public string TeamMotto { get; set; }
    public Color Color { get; set; }

    public bool LeftSide { get; set; }
    public int Score { get; set; }

    // Fans
    public int Goblins { get; set; }
    public int Trolle { get; set; }
    public int Elfen { get; set; }
    public int Niffler { get; set; }
    public int Wombats { get; set; }

    public List<Wizard> wizards = new List<Wizard>();
    public List<Fan> fans = new List<Fan>();

    public Team(TeamConfig teamconfig, int teamNr)
    {
        TeamName = "Atleast";
        TeamMotto = "we tried :(";
        Color = Color.grey;

        LeftSide = true;
        Score = 0;

        Goblins = 3;
        Trolle = 1;
        Elfen = 1;
        Niffler = 1;
        Wombats = 1;

        Vector3Int vector3 = new Vector3Int(-1, -1, 0);
        // Fill list with wizards
        wizards.Add(new Wizard(teamNr, vector3, WizardRole.Jäger, Besen.cleansweep11, "chaser1", "Name 1", "w"));
        wizards.Add(new Wizard(teamNr, vector3, WizardRole.Jäger, Besen.comet260, "chaser2", "Name 2", "w"));
        wizards.Add(new Wizard(teamNr, vector3, WizardRole.Jäger, Besen.firebolt, "chaser3", "Name 3", "w"));
        wizards.Add(new Wizard(teamNr, vector3, WizardRole.Treiber, Besen.nimbus2001, "beater1", "Name 4", "w"));
        wizards.Add(new Wizard(teamNr, vector3, WizardRole.Treiber, Besen.tinderblast, "beater2", "Name 5", "w"));
        wizards.Add(new Wizard(teamNr, vector3, WizardRole.Hüter, Besen.cleansweep11, "keeper", "Name 6", "w"));
        wizards.Add(new Wizard(teamNr, vector3, WizardRole.Sucher, Besen.cleansweep11, "seeker", "Name 7", "w"));

        for (int i = 0; i < 7; i++)
        {
            if (Goblins > 0)
            {
                fans.Add(new Fan(FanRole.Goblins, false));
                Goblins--;
            }
            else if (Trolle > 0)
            {
                fans.Add(new Fan(FanRole.Trolle, false));
                Trolle--;
            }
            else if (Elfen > 0)
            {
                fans.Add(new Fan(FanRole.Elfen, false));
                Elfen--;
            }
            else if (Niffler > 0)
            {
                fans.Add(new Fan(FanRole.Niffler, false));
                Niffler--;
            }
            else if (Wombats > 0)
            {
                fans.Add(new Fan(FanRole.Wombats, false));
                Wombats--;
            }
        }

        //Nullpointerexceptions da keine standard Jason
        //wizards.Add(new Wizard(vector3, WizardRole.Jäger, convertBesen(teamconfig.players.Jäger1.broom), teamconfig.players.Jäger1.name, teamconfig.players.Jäger1.sex));
        //wizards.Add(new Wizard(vector3, WizardRole.Jäger, convertBesen(teamconfig.players.Jäger2.broom), teamconfig.players.Jäger2.name, teamconfig.players.Jäger2.sex));
        //wizards.Add(new Wizard(vector3, WizardRole.Jäger, convertBesen(teamconfig.players.Jäger3.broom), teamconfig.players.Jäger3.name, teamconfig.players.Jäger3.sex));
        //wizards.Add(new Wizard(vector3, WizardRole.Treiber, convertBesen(teamconfig.players.Treiber1.broom), teamconfig.players.Treiber1.name, teamconfig.players.Treiber1.sex));
        //wizards.Add(new Wizard(vector3, WizardRole.Treiber, convertBesen(teamconfig.players.Treiber2.broom), teamconfig.players.Treiber2.name, teamconfig.players.Treiber2.sex));
        //wizards.Add(new Wizard(vector3, WizardRole.Hüter, convertBesen(teamconfig.players.Hüter.broom), teamconfig.players.Hüter.name, teamconfig.players.Hüter.sex));
        //wizards.Add(new Wizard(vector3, WizardRole.Sucher, convertBesen(teamconfig.players.Sucher.broom), teamconfig.players.Sucher.name, teamconfig.players.Sucher.sex));


    }

    Besen ConvertBesen(string besen)
    {
        switch(besen)
        {
            case "tinderblast":
                return Besen.tinderblast;
            case "cleansweep11":
                return Besen.cleansweep11;
            case "comet260":
                return Besen.comet260;
            case "nimbus2001":
                return Besen.nimbus2001;
            case "firebolt":
                return Besen.firebolt;
            default:
                //something went wrong, shit, lets play it off cool
                return Besen.tinderblast;
        }
    }
}
