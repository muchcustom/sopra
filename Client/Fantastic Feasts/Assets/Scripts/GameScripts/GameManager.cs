﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Tilemaps;

public enum Phase
{
    Default, Teamformations, Ball, Spieler, Fan
}

public class GameManager : MonoBehaviour
{
    // Size of the field
    public static readonly int numberColumns = 17;
    public static readonly int numberRows = 13;

    /*
     * Unity
     */
    public static readonly string whiteTile = "Tileset64Pixel_0";

    public Grid grid;
    public Tilemap groundTilemap;
    public Tilemap activeTilemap;
    public Tilemap playersTilemap;
    public Tilemap fansTilemap;

    public List<Tile> groundTiles; // Stores the groundTiles

    public List<Tile> wizardTiles; // 0: Keeper, 1: Chaser, 2: Seeker, 3: Beater
    public List<Tile> fanTiles;
    public List<Tile> ballTiles; // 0: Snitch, 1: NAME, 2: Quaffle

    // Field marks
    public Tile activeField;
    public Tile neighborField;

    // Text
    public Text phasenText;
    public Text team1NameText;
    public Text team1ScoreText;
    public Text team2NameText;
    public Text team2ScoreText;
    public Text informationText;


    /*
     * Game Logic
     */

    public Team team1; // This client team
    public Team team2; // Opponent team

    public List<Ball> balls; // NOT USED YET

    public Wizard clickedWizard; // Focussed wizard
    public bool isSpectator = false;
    public Phase phase;

    void Start()
    {
        phase = Phase.Teamformations;

        // Initiate teams
        LoadTeamScript loadIt = new LoadTeamScript();
        TeamConfig teamconfig1 = loadIt.getConfig();
        // Teamconfig2 muss der aus der JSON des Gegners erstellt werden
        // Momentan spielt auch der Gegner mit der teamconfig unseres Clienten
        TeamConfig teamconfig2 = loadIt.getConfig();

        team1 = new Team(teamconfig1, 1);
        team1.Score = 420;
        team1.TeamName = "Team Gewinner";
        team1.Color = Color.blue;
        // Q: Can you see the opponent's team while prep?
        team2 = new Team(teamconfig2, 2);
        team2.Score = 69;
        team2.TeamName = "Team Verlierer";
        team2.Color = Color.magenta;

        // Set visuals for the teams
        SetWizardTiles(team1, wizardTiles);
        SetWizardTiles(team2, wizardTiles);

        SetupText();

        Formation();

        UpdateTeamColors();
    }

    // Update is called once per frame
    void Update()
    {
        DetectInput();
    }

    /*
     * Input Methods
     */

    void DetectInput()
    {
        if (isSpectator)
        {
            // Dont deal with spectator clicks
            Debug.Log("Spectator Mode is active");
            return;
        }

        // TODO Note for user
        // RightClick to unselect
        if (Input.GetMouseButtonDown(1))
        {
            clickedWizard = null;
            activeTilemap.ClearAllTiles();
        }

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int coordinates = grid.WorldToCell(mouseWorldPos);
            Debug.Log("Click position: " + coordinates);

            SetTileActive(coordinates);

            var clickedObject = GetFieldObject(coordinates.x, coordinates.y);
            //Debug.Log("Clicked tile: " + clickedTile);

            if (clickedObject != null)
            {
                if (clickedObject.GetType() == typeof(Wizard))
                {
                    clickedWizard = (Wizard) clickedObject;
                    SetInformationText("Team: " + clickedWizard.TeamNr + "\nName: " + clickedWizard.Playername + "\nRolle: " + clickedWizard.WizardRole + "\nBesen: " + clickedWizard.Besen + "\nPosition: " + clickedWizard.Position, false);
                    // TODO only temp start
                    if (phase == Phase.Spieler)
                    {
                        SetNeighborTiles(coordinates.x, coordinates.y);
                    }
                    // TODO only temp end
                }
                else if (clickedObject.GetType() == typeof(Ball)) // NOT TESTED
                {
                    Ball clickedBall = (Ball) clickedObject;
                    SetInformationText("Ball: " + clickedBall.BallType + "\nPosition: " + clickedBall.Position, false);
                }
            }
            else
            {
                if (phase == Phase.Teamformations)
                {
                    if (!IsInGroundTilemap(coordinates.x, coordinates.y))
                    {
                        return;
                    }

                    if (clickedWizard != null)
                    {
                        FormationMovePlayer(coordinates);
                    }
                }
                else if (phase == Phase.Spieler)
                {
                    // If nothing is on the field, move the player
                    if (GetFieldObject(coordinates.x, coordinates.y) == null)
                    {
                        MovePlayer(coordinates);
                    }
                }
                else
                {
                    //clickedPlayer = null;
                    SetInformationText("Click position: " + coordinates, false);
                }
            }
        }
    }

    /*
     * Setup Methods
     */

    // Sets the tiles (visuals) for a team
    void SetWizardTiles(Team team, List<Tile> tiles)
    {
        for (int i = 0; i < team.wizards.Count; i++)
        {
            Wizard wizard = team.wizards[i];
            switch (wizard.WizardRole)
            {
                case WizardRole.Hüter:
                    wizard.Tile = tiles[0];
                    break;
                case WizardRole.Jäger:
                    wizard.Tile = tiles[1];
                    break;
                case WizardRole.Sucher:
                    wizard.Tile = tiles[2];
                    break;
                case WizardRole.Treiber:
                    wizard.Tile = tiles[3];
                    break;
            }
        }
    }

    // Updates the team tile colors (TODO rgb )
    void UpdateTeamColors()
    {
        // Team 1 groundTiles
        List<Vector3Int> team1Tiles = new List<Vector3Int>() {
            new Vector3Int(-2, -3, 0),
            new Vector3Int(-2, -4, 0),
            new Vector3Int(-2, -5, 0),
            new Vector3Int(-2, -6, 0),
            new Vector3Int(-2, -7, 0),
            new Vector3Int(-2, -8, 0),
            new Vector3Int(-2, -9, 0),

            new Vector3Int(3, 0, 0),
            new Vector3Int(4, 0, 0),
            new Vector3Int(5, 0, 0),
            new Vector3Int(6, 0, 0),
            new Vector3Int(7, 0, 0),
            new Vector3Int(3, -1, 0),
            new Vector3Int(4, -1, 0),
            new Vector3Int(5, -1, 0),
            new Vector3Int(6, -1, 0),
            new Vector3Int(7, -1, 0),
            new Vector3Int(4, -2, 0),
            new Vector3Int(5, -2, 0),
            new Vector3Int(6, -2, 0),
            new Vector3Int(7, -2, 0),
            new Vector3Int(4, -3, 0),
            new Vector3Int(5, -3, 0),
            new Vector3Int(6, -3, 0),
            new Vector3Int(7, -3, 0),
            new Vector3Int(5, -4, 0),
            new Vector3Int(6, -4, 0),
            new Vector3Int(7, -4, 0),
            new Vector3Int(5, -5, 0),
            new Vector3Int(6, -5, 0),
            new Vector3Int(5, -6, 0),
            new Vector3Int(6, -6, 0),
            new Vector3Int(5, -7, 0),
            new Vector3Int(6, -7, 0),
            new Vector3Int(5, -8, 0),
            new Vector3Int(6, -8, 0),
            new Vector3Int(7, -8, 0),
            new Vector3Int(4, -9, 0),
            new Vector3Int(5, -9, 0),
            new Vector3Int(6, -9, 0),
            new Vector3Int(7, -9, 0),
            new Vector3Int(4, -10, 0),
            new Vector3Int(5, -10, 0),
            new Vector3Int(6, -10, 0),
            new Vector3Int(7, -10, 0),
            new Vector3Int(3, -11, 0),
            new Vector3Int(4, -11, 0),
            new Vector3Int(5, -11, 0),
            new Vector3Int(6, -11, 0),
            new Vector3Int(7, -11, 0),
            new Vector3Int(3, -12, 0),
            new Vector3Int(4, -12, 0),
            new Vector3Int(5, -12, 0),
            new Vector3Int(6, -12, 0),
            new Vector3Int(7, -12, 0),
        };

        foreach (Vector3Int pos in team1Tiles)
        {
            groundTilemap.SetTileFlags(pos, TileFlags.None);

            // Set the colour.
            groundTilemap.SetColor(pos, team1.Color);
        }

        // Team 1 wizards
        foreach (Wizard wizard in team1.wizards)
        {
            playersTilemap.SetTileFlags(wizard.Position, TileFlags.None);

            // Set the colour.
            playersTilemap.SetColor(wizard.Position, team1.Color);
        }

        // Team 1 fans
        foreach (Fan fan in team1.fans)
        {
            fansTilemap.SetTileFlags(fan.Position, TileFlags.None);

            // Set the colour.
            fansTilemap.SetColor(fan.Position, team1.Color);
        }


        // Team 2 groundTiles
        List<Vector3Int> team2Tiles = new List<Vector3Int>() {
            new Vector3Int(18, -3, 0),
            new Vector3Int(18, -4, 0),
            new Vector3Int(18, -5, 0),
            new Vector3Int(18, -6, 0),
            new Vector3Int(18, -7, 0),
            new Vector3Int(18, -8, 0),
            new Vector3Int(18, -9, 0),

            new Vector3Int(9, 0, 0),
            new Vector3Int(10, 0, 0),
            new Vector3Int(11, 0, 0),
            new Vector3Int(12, 0, 0),
            new Vector3Int(13, 0, 0),
            new Vector3Int(9, -1, 0),
            new Vector3Int(10, -1, 0),
            new Vector3Int(11, -1, 0),
            new Vector3Int(12, -1, 0),
            new Vector3Int(13, -1, 0),
            new Vector3Int(9, -2, 0),
            new Vector3Int(10, -2, 0),
            new Vector3Int(11, -2, 0),
            new Vector3Int(12, -2, 0),
            new Vector3Int(9, -3, 0),
            new Vector3Int(10, -3, 0),
            new Vector3Int(11, -3, 0),
            new Vector3Int(12, -3, 0),
            new Vector3Int(9, -4, 0),
            new Vector3Int(10, -4, 0),
            new Vector3Int(11, -4, 0),
            new Vector3Int(10, -5, 0),
            new Vector3Int(11, -5, 0),
            new Vector3Int(10, -6, 0),
            new Vector3Int(11, -6, 0),
            new Vector3Int(10, -7, 0),
            new Vector3Int(11, -7, 0),
            new Vector3Int(9, -8, 0),
            new Vector3Int(10, -8, 0),
            new Vector3Int(11, -8, 0),
            new Vector3Int(9, -9, 0),
            new Vector3Int(10, -9, 0),
            new Vector3Int(11, -9, 0),
            new Vector3Int(12, -9, 0),
            new Vector3Int(9, -10, 0),
            new Vector3Int(10, -10, 0),
            new Vector3Int(11, -10, 0),
            new Vector3Int(12, -10, 0),
            new Vector3Int(9, -11, 0),
            new Vector3Int(10, -11, 0),
            new Vector3Int(11, -11, 0),
            new Vector3Int(12, -11, 0),
            new Vector3Int(13, -11, 0),
            new Vector3Int(9, -12, 0),
            new Vector3Int(10, -12, 0),
            new Vector3Int(11, -12, 0),
            new Vector3Int(12, -12, 0),
            new Vector3Int(13, -12, 0),
        };

        foreach (Vector3Int pos in team2Tiles)
        {
            groundTilemap.SetTileFlags(pos, TileFlags.None);

            // Set the colour.
            groundTilemap.SetColor(pos, team2.Color);
        }

        // Team 2 wizards
        foreach (Wizard wizard in team2.wizards)
        {
            playersTilemap.SetTileFlags(wizard.Position, TileFlags.None);

            // Set the colour.
            playersTilemap.SetColor(wizard.Position, team2.Color);
        }

        // Team 2 fans
        foreach (Fan fan in team2.fans)
        {
            fansTilemap.SetTileFlags(fan.Position, TileFlags.None);

            // Set the colour.
            fansTilemap.SetColor(fan.Position, team2.Color);
        }
    }

    void SetupText()
    {
        team1NameText.color = team1.Color;
        team2NameText.color = team2.Color;

        team1NameText.text = team1.TeamName;
        team2NameText.text = team2.TeamName;
        //team1NameText.text = team1.TeamName + "\n" + team1.TeamMotto; // with motto
        //team2NameText.text = team2.TeamName + "\n" + team1.TeamMotto; // with motto

        team1ScoreText.text = team1.Score.ToString();
        team2ScoreText.text = team2.Score.ToString();

        phasenText.text = phase.ToString() + "phase";
    }

    Tile GetFanTile(int teamNr, Fan fan)
    {
        Tile tile;

        switch (fan.FanRole)
        {
            case FanRole.Niffler:
                tile = fanTiles[0];
                break;
            case FanRole.Elfen:
                tile = fanTiles[1];
                break;
            case FanRole.Goblins:
                tile = fanTiles[2];
                break;
            case FanRole.Wombats:
                tile = fanTiles[3];
                break;
            case FanRole.Trolle:
                tile = fanTiles[4];
                break;
            default:
                // Es ist Michis Fehler :^)
                tile = fanTiles[0];
                break;
        }
        return tile;
    }

    void Formation()
    {
        // Wizards
        Vector3Int[] wizardPosition1 = { new Vector3Int(6, -5, 0),
                                         new Vector3Int(6, -6, 0),
                                         new Vector3Int(6, -7, 0),
                                         new Vector3Int(4, -5, 0),
                                         new Vector3Int(4, -7, 0),
                                         new Vector3Int(3, -6, 0),
                                         new Vector3Int(0, -6, 0)};


        Vector3Int[] wizardPosition2 = { new Vector3Int(10, -5, 0),
                                         new Vector3Int(10, -6, 0),
                                         new Vector3Int(10, -7, 0),
                                         new Vector3Int(12, -5, 0),
                                         new Vector3Int(12, -7, 0),
                                         new Vector3Int(13, -6, 0),
                                         new Vector3Int(16, -6, 0)};

        for (int i = 0; i < 7; i++)
        {
            team1.wizards[i].Position = wizardPosition1[i];
            playersTilemap.SetTile(wizardPosition1[i], team1.wizards[i].Tile);

            team2.wizards[i].Position = wizardPosition2[i];
            playersTilemap.SetTile(wizardPosition2[i], team2.wizards[i].Tile);
        }


        //Fans
        Vector3Int[] positions1 = { new Vector3Int(-2, -3, 0),
                                    new Vector3Int(-2, -4, 0),
                                    new Vector3Int(-2, -5, 0),
                                    new Vector3Int(-2, -6, 0),
                                    new Vector3Int(-2, -7, 0),
                                    new Vector3Int(-2, -8, 0),
                                    new Vector3Int(-2, -9, 0)};



        Vector3Int[] positions2 = { new Vector3Int(18, -3, 0),
                                    new Vector3Int(18, -4, 0),
                                    new Vector3Int(18, -5, 0),
                                    new Vector3Int(18, -6, 0),
                                    new Vector3Int(18, -7, 0),
                                    new Vector3Int(18, -8, 0),
                                    new Vector3Int(18, -9, 0)};

        for (int i = 0; i < 7; i++)
        {
            team1.fans[i].Position = positions1[i];
            team1.fans[i].Tile = GetFanTile(1, team1.fans[i]);
            fansTilemap.SetTile(positions1[i], team1.fans[i].Tile);

            team2.fans[i].Position = positions2[i];
            team2.fans[i].Tile = GetFanTile(2, team2.fans[i]);
            fansTilemap.SetTile(positions2[i], team2.fans[i].Tile);
        }
    }

    /*
     * Visual Methods
     */

    void SetInformationText(string newText, bool isWarning)
    {
        if (isWarning)
        {
            informationText.color = Color.red;
        }
        else
        {
            informationText.color = Color.black;
        }
        informationText.text = newText;
    }

    // Marks the currently clicked tile
    void SetTileActive(Vector3Int vector)
    {
        // Reset 
        activeTilemap.ClearAllTiles();

        // Check if groundTilemap was clicked
        if (!IsInGroundTilemap(vector.x, vector.y))
        {
            return;
        }

        // Set active field
        activeTilemap.SetTile(vector, activeField);
    }

    // Is called to mark the neighborTiles of a wizard
    void SetNeighborTiles(int x, int y)
    {
        for (int xPos = -1; xPos < 2; xPos++)
        {
            for (int yPos = -1; yPos < 2; yPos++)
            {
                int currentX = x + xPos;
                int currentY = y + yPos;
                if (IsInGroundTilemap(currentX, currentY))
                {
                    Vector3Int tilePos = new Vector3Int(currentX, currentY, 0);
                    TileBase tile = groundTilemap.GetTile(tilePos);

                    // Mark clicked tile with activeField
                    if (currentX == x && currentY == y)
                    {
                        activeTilemap.SetTile(tilePos, activeField);
                        continue;
                    }

                    if (IsNeighborTile(tile, tilePos))
                    {
                        // Mark as neighbor
                        activeTilemap.SetTile(tilePos, neighborField);
                    }
                }
            }
        }
    }

    /*
     * Join
     */
    void SendJoinRequest()
    {
        /* Send JSON Join Request
        {
          "lobby": "(string/lobbyType)",
          "userName": "(string/userNameType)",
          "password" : "(string)",
          "isArtificialIntelligence":"(boolean)",
          "mods":["(string)"]
        }
        */
    }

    /*
     * Preparation Phase
     */
    void SendTeamFormation()
    {
        // Create and send JSON with positions
        /* TeamFormation
         {
          "players": {
            "seeker": {
              "xPos": "(int/posx)",
              "yPos": "(int/posy)"
            },
            "keeper": {
              "xPos": "(int/posx)",
              "yPos": "(int/posy)"
            },
            "chaser1": {
              "xPos": "(int/posx)",
              "yPos": "(int/posy)"
            },
            "chaser2": {
              "xPos": "(int/posx)",
              "yPos": "(int/posy)"
            },
            "chaser3": {
              "xPos": "(int/posx)",
              "yPos": "(int/posy)"
            },
            "beater1": {
              "xPos": "(int/posx)",
              "yPos": "(int/posy)"
            },
            "beater2": {
              "xPos": "(int/posx)",
              "yPos": "(int/posy)"
            }
          }
        }
        */
    }

    /*
     * Movement
     */

    // Moves the currently selected wizard to the vector's coordinates in the PlayerPhase
    void MovePlayer(Vector3Int vector)
    {
        if (IsClickedNeighbor(vector.x, vector.y))
        {
            var tile = GetFieldObject(vector.x, vector.y);

            // Check if the tile is already occupied
            if (tile != null)
            {
                // The tile already has something on it.
                // TODO what should happen?
                activeTilemap.ClearAllTiles();
                return;
            }

            // Check if the click is still in the field
            if (groundTilemap.GetTile(vector).name == whiteTile)
            {
                activeTilemap.ClearAllTiles();
                return;
            }

            // Remove player from previous position
            playersTilemap.SetTile(clickedWizard.Position, null);

            // Set new position
            clickedWizard.Position = vector;
            playersTilemap.SetTile(vector, clickedWizard.Tile);

            // Set flag
            playersTilemap.SetTileFlags(vector, TileFlags.None);

            // Set the colour.
            playersTilemap.SetColor(vector, team1.Color);

            // Reset
            clickedWizard = null;
            activeTilemap.ClearAllTiles();
            SetInformationText("", false);
        }
        else
        {
            activeTilemap.ClearAllTiles();
        }
    }

    // Moves the currently selectes wizard to the vector's coordinates in the TeamFormationPhase
    void FormationMovePlayer(Vector3Int vector)
    {
        var tile = GetFieldObject(vector.x, vector.y);

        Debug.Log(clickedWizard);

        // Check if the tile is already occupied
        if (tile != null)
        {
            // The tile already has something on it.
            // TODO what should happen?
            activeTilemap.ClearAllTiles();
            return;
        }

        // Check if the click is still in the field
        if (groundTilemap.GetTile(vector).name == whiteTile)
        {
            activeTilemap.ClearAllTiles();
            return;
        }


        // Check whether the right field half was clicked
        int xLeft;
        int xRight;
        if (team1.LeftSide)
        {
            xLeft = 0;
            xRight = 8;
        }
        else
        {

            xLeft = 9;
            xRight = 17;
        }
        if (!(xLeft <= vector.x && vector.x < xRight && 0 >= vector.y && vector.y > -13))
        {
            Debug.Log("Clicked tile is not in the player's field half.");
            SetInformationText("Bitte nicht schummeln :(\nPlatziere deine Zauberer auf deiner Hälfte", true);
            return;
        }

        // Check whether field is torRing or not
        if (IsTorRingField(vector))
        {
            Debug.Log("Can't move to torRingField.");
            SetInformationText("Du kannst deine Zauberer nicht auf einem Torringfeld positionieren.", true);
            return;
        }

        // Check whether field is a middleTile
        List<Vector3Int> middleFieldPositions = new List<Vector3Int>() {
                new Vector3Int(7, -5, 0),
                new Vector3Int(7, -6, 0),
                new Vector3Int(7, -7, 0),
                new Vector3Int(9, -5, 0),
                new Vector3Int(9, -6, 0),
                new Vector3Int(9, -7, 0),
        };
        foreach (Vector3Int pos in middleFieldPositions)
        {
            if (pos == vector)
            {
                SetInformationText("Du kannst deine Zauberer nicht im Mittelkreis positionieren.", true);
                return;
            }
        }

        // Remove player from previous position
        playersTilemap.SetTile(clickedWizard.Position, null);

        // Set new position
        clickedWizard.Position = vector;
        playersTilemap.SetTile(vector, clickedWizard.Tile);

        // Set flag
        playersTilemap.SetTileFlags(vector, TileFlags.None);

        // Set the colour.
        playersTilemap.SetColor(vector, team1.Color);

        SetInformationText("", false);
    }

    /*
     * Checking Methods
     */

    // X and y are the coordinates of the click
    bool IsClickedNeighbor(int x, int y)
    {
        // If no player was previously clicked, then skip
        if (clickedWizard == null)
        {
            return false;
        }

        if (!IsInGroundTilemap(x, y))
        {
            return false;
        }

        Vector3Int wizardPos = clickedWizard.Position;
        if ((x == wizardPos.x - 1 || x == wizardPos.x || x == wizardPos.x + 1) && (y == wizardPos.y - 1 || y == wizardPos.y || y == wizardPos.y + 1))
        {
            //Debug.Log("The clicked tile is a neighbor");
            return true;
        }
        else
        {
            //Debug.Log("The clicked tile is not a neighbor");
            return false;
        }
    }

    bool IsInGroundTilemap(int x, int y)
    {
        if (0 <= x && x < numberColumns && 0 >= y && y > -numberRows)
        {
            //Debug.Log("Click was in the groundTileMap.");
            return true;
        }
        else
        {
            Debug.Log("Click was not in the groundTileMap.");
            return false;
        }
    }

    bool IsTorRingField(Vector3Int position)
    {
        List<Vector3Int> torRingPositions = new List<Vector3Int>() {
            // Left side
            new Vector3Int(2, -4, 0),
            new Vector3Int(2, -6, 0),
            new Vector3Int(2, -8, 0),
            // Right side
            new Vector3Int(14, -4, 0),
            new Vector3Int(14, -6, 0),
            new Vector3Int(14, -8, 0),
        };

        // Iterate through torRingPositions
        foreach (Vector3Int torRingPos in torRingPositions)
        {
            if (position == torRingPos)
            {
                return true;
            }
        }

        return false;
    }

    // Checks whether something is already on the tile
    // If something is on the tile, it will be returned
    // Returns null when nothing is on the tile
    dynamic GetFieldObject(int xPos, int yPos)
    {
        // Iterate through team1
        for (int i = 0; i < team1.wizards.Count; i++)
        {
            Vector3Int pos = team1.wizards[i].Position;
            if (pos.x == xPos && pos.y == yPos)
            {
                return team1.wizards[i];
            }
        }

        // Iterate through team2
        for (int i = 0; i < team2.wizards.Count; i++)
        {
            Vector3Int pos = team2.wizards[i].Position;
            if (pos.x == xPos && pos.y == yPos)
            {
                return team2.wizards[i];
            }
        }

        // Iterate through balls
        // TODO

        return null;
    }

    bool IsNeighborTile(TileBase tile, Vector3Int pos)
    {
        Debug.Log(tile);

        if (tile == null)
        {
            return false;
        }

        if (GetFieldObject(pos.x, pos.y) != null)
        {
            return false;
        }

        if (tile.name == whiteTile)
        {
            return false;
        }

        return true;
    }
}