﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crosstales.FB;
using UnityEngine.UI;

public class LoadTeamScript : MonoBehaviour
{
    /// <summary>
    /// Spieler
    /// </summary>
    private JSONPlayer hueter = new JSONPlayer();
    private JSONPlayer sucher = new JSONPlayer();
    private JSONPlayer firstjaeger = new JSONPlayer();
    private JSONPlayer secondjaeger = new JSONPlayer();
    private JSONPlayer thirdjaeger = new JSONPlayer();
    private JSONPlayer firsttreiber = new JSONPlayer();
    private JSONPlayer secondtreiber = new JSONPlayer();


    //TeamConfig Objekt
    private TeamConfig newTeamConfig = new TeamConfig();

    //Teamobjekt
    private JSONTeam team = new JSONTeam();

    //Fans
    private JSONFans fans = new JSONFans();

    //Farben
    private JSONColors colors = new JSONColors();

    //Teaminput
    string path;

    //TeamJSON
    string teamJSON;


    //Textfeld
    public Text teamloadtext;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //FileExplorer
    public void OpenExplorer()
    {
        path = FileBrowser.OpenSingleFile("txt");
        GetTeam();
    }

    void GetTeam()
    {
        if (path != null)
        {
            UpdateTeam();
        }
    }

    void UpdateTeam()
    {
        WWW www = new WWW("file:///" + path);
        teamJSON = www.text;

        newTeamConfig = JsonUtility.FromJson<TeamConfig>(teamJSON);

        //setValues();

        teamloadtext.text = "Team erfolgreich geladen!";
    }

    //Werte werden gesetzt 
    void setValues()
    {
        sucher = team.Sucher;
        hueter = team.Hüter;
        firstjaeger = team.Jäger1;
        secondjaeger = team.Jäger2;
        thirdjaeger = team.Jäger3;
        firsttreiber = team.Treiber1;
        secondjaeger = team.Treiber2;
    }

    //Auf Anfrage wird die Config zurückgegeben
    public TeamConfig getConfig()
    {
        return this.newTeamConfig;
    }
}



//Team-Config Klasse für JSON 
[System.Serializable]
public class TeamConfig
{
    public string name;
    public string motto;
    public JSONColors colors;
    public string image;
    public JSONTeam players;
    public JSONFans fans;
}



//Teamklasse
[System.Serializable]
public class JSONTeam
{
    public JSONPlayer Sucher;
    public JSONPlayer Hüter;
    public JSONPlayer Jäger1;
    public JSONPlayer Jäger2;
    public JSONPlayer Jäger3;
    public JSONPlayer Treiber1;
    public JSONPlayer Treiber2;
}



//Spielerklasse
[System.Serializable]
public class JSONPlayer
{
    public string name;
    public string broom;
    public string sex;
}



//Fansklasse
[System.Serializable]
public class JSONFans
{
    public int goblins;
    public int trolls;
    public int elves;
    public int nifflers;
}


//Farbenklasse
[System.Serializable]
public class JSONColors
{
    public string primary;
    public string secondary;
}
